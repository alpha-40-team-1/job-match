import unittest
from unittest.mock import Mock
from routers import skills as skill_router
from data.models import SkillResponse, AdSkillsetResponse, Level

mock_skill_service = Mock(spec='services.skill_service')
skill_router.skill_service = mock_skill_service
mock_auth = Mock()

def fake_ad_skillset_response(category_name = 'programming', skillset = []):
    mock_ad_skillset_response = Mock(spec=AdSkillsetResponse)
    mock_ad_skillset_response.category_name = category_name
    mock_ad_skillset_response.skillset = skillset

def fake_skill_response(id = 1, name = 'python', level = []):
    mock_skill_response = Mock(spec=SkillResponse)
    mock_skill_response.id = id
    mock_skill_response.name = name
    mock_skill_response.level = level

def fake_level(id = 1, level_name = 'junior developer'):
    mock_level = Mock(spec=Level)
    mock_level.id = id
    mock_level.level_name = level_name

class SkillRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_skill_service.reset_mock()
    
    def test_getSkillsAndDetails_returns_AdSkillsetResponse(self):
        pass