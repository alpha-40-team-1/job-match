import unittest
from unittest.mock import Mock
from data.models import JobAd, JobAdResponse
from services import job_ad_service
from routers import job_ads as job_ads_router
from common.responses import NotFound, NoContent, Unauthorized
import jwt

mock_job_ad_service = Mock('services.job_ad_service')
mock_auth = Mock()

def fake_job_ad(id = 1, min_salary = 1000, max_salary = 2000, description = 'mock description', remote = True, status = True, company_id = 2, city_id = 3):
    mock_job_ad = Mock(spec = JobAd)
    mock_job_ad.id = id
    mock_job_ad.min_salary = min_salary
    mock_job_ad.max_salary = max_salary
    mock_job_ad.description = description
    mock_job_ad.remote = remote
    mock_job_ad.status = status
    mock_job_ad.company_id = company_id
    mock_job_ad.city_id = city_id
    return mock_job_ad



class JobAdServicesShould(unittest.TestCase):

    def set_up(self):
        mock_job_ad_service.reset_mock()
        


    def update_returns_NotFound_when_no_id_is_founf(self):
        job_ad = fake_job_ad()
        mock_auth.return_value = job_ad
        mock_job_ad_service.get_by_id = lambda id: None

        self.assertEqual(NotFound, type(job_ads_router.update_job_ad(2, job_ad = job_ad)))

    def test_get_by_id_returns_empty_generator_when_no_Job_ad(self):
        mock_job_ad_service.get_by_id = lambda id : None
        generator = mock_job_ad_service.get_by_id(id=)
        list_from_generator = list(generator)
        self.assertEqual(0, len(list_from_generator))

    def test_getById_returns_None_when_noJobAd(self):
        mock_job_ad_service = lambda id, table: None

        self.assertEqual(None, job_ad_service.get_by_id(id=1))


    def test_all_returns_empty_generator_when_no_Job_ad(self):
        mock_job_ad_service.get_by_id = lambda id, table: None
        generator = job_ad_service.all()
        list_from_generator = list(generator)
        self.assertEqual(0, len(list_from_generator))
    