-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: job_match_api
-- ------------------------------------------------------
-- Server version	5.5.5-10.9.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ad_states`
--

DROP TABLE IF EXISTS `ad_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ad_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_states`
--

LOCK TABLES `ad_states` WRITE;
/*!40000 ALTER TABLE `ad_states` DISABLE KEYS */;
INSERT INTO `ad_states` VALUES (1,'active'),(2,'hidden'),(4,'matched'),(3,'private');
/*!40000 ALTER TABLE `ad_states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'admin','admin');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `approvals`
--

DROP TABLE IF EXISTS `approvals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `approvals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `name` varchar(256) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `approvals`
--

LOCK TABLES `approvals` WRITE;
/*!40000 ALTER TABLE `approvals` DISABLE KEYS */;
INSERT INTO `approvals` VALUES (1,'skill','japanese',2);
/*!40000 ALTER TABLE `approvals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Aheloy'),(2,'Ahtopol'),(3,'Aksakovo'),(4,'Alfatar'),(5,'Anton'),(6,'Antonovo'),(7,'Apriltsi'),(8,'Ardino'),(9,'Asen'),(10,'Asenovgrad'),(11,'Aytos'),(12,'Balchik'),(13,'Balgarevo'),(14,'Banite'),(15,'Bansko'),(16,'Bata'),(17,'Batak'),(18,'Batanovtsi'),(19,'Belene'),(20,'Belitsa'),(21,'Belogradchik'),(22,'Beloslav'),(23,'Belovo'),(24,'Berkovitsa'),(25,'Blagoevgrad'),(26,'Boboshevo'),(27,'Bobov Dol'),(28,'Bolyarovo'),(29,'Borino'),(30,'Borovan'),(31,'Borovo'),(32,'Botevgrad'),(33,'Bov'),(34,'Boychinovtsi'),(35,'Boynitsa'),(36,'Bozhurishte'),(37,'Bratsigovo'),(38,'Bregovo'),(39,'Breznik'),(40,'Brezovo'),(41,'Brusartsi'),(42,'Buhovo'),(43,'Burgas'),(44,'Byala'),(45,'Byala Cherkva'),(46,'Byala Slatina'),(47,'Chavdar'),(48,'Chelopech'),(49,'Chepelare'),(50,'Chernomorets'),(51,'Cherven Bryag'),(52,'Chiprovtsi'),(53,'Chirpan'),(54,'Chuprene'),(55,'Dalgopol'),(56,'Debelets'),(57,'Devin'),(58,'Devnya'),(59,'Dimitrovgrad'),(60,'Dimovo'),(61,'Dobrich'),(62,'Dolna Banya'),(63,'Dolna Mitropolia'),(64,'Dolni Chiflik'),(65,'Dolni Dabnik'),(66,'Dospat'),(67,'Dragoman'),(68,'Drenovets'),(69,'Dryanovo'),(70,'Dulovo'),(71,'Dunavtsi'),(72,'Dupnitsa'),(73,'Dve Mogili'),(74,'Dzhebel'),(75,'Elena'),(76,'Elhovo'),(77,'Elin Pelin'),(78,'Elkhovo'),(79,'Etropole'),(80,'Gabrovo'),(81,'Gara Hitrino'),(82,'Garmen'),(83,'General Toshevo'),(84,'Glavinitsa'),(85,'Godech'),(86,'Gorichevo'),(87,'Gorna Malina'),(88,'Gorna Oryahovitsa'),(89,'Gotse Delchev'),(90,'Gramada'),(94,'Gŭlŭbovo'),(91,'Gulyantsi'),(92,'Gurkovo'),(93,'Gyovren'),(95,'Hadzhidimovo'),(96,'Harmanli'),(97,'Haskovo'),(98,'Hayredin'),(99,'Hisarya'),(100,'Ihtiman'),(101,'Iskar'),(102,'Isperih'),(103,'Ivanovo'),(104,'Ivaylovgrad'),(105,'Kalofer'),(106,'Kaloyanovo'),(107,'Kameno'),(108,'Kaolinovo'),(109,'Kardzhali'),(110,'Karlovo'),(111,'Karnobat'),(112,'Kaspichan'),(113,'Kavarna'),(114,'Kaynardzha'),(115,'Kazanlak'),(116,'Kermen'),(117,'Kilifarevo'),(118,'Kirkovo'),(119,'Kiten'),(120,'Klisura'),(121,'Knezha'),(122,'Kocherinovo'),(123,'Koprivshtitsa'),(124,'Kostinbrod'),(125,'Kotel'),(126,'Koynare'),(127,'Kozloduy'),(128,'Kresna'),(129,'Krichim'),(130,'Krivodol'),(131,'Krumovgrad'),(132,'Krushari'),(133,'Kubrat'),(134,'Kula'),(135,'Kyustendil'),(136,'Lakatnik'),(137,'Laki'),(138,'Lesichovo'),(139,'Letnitsa'),(140,'Levski'),(141,'Lom'),(142,'Lovech'),(143,'Loznitsa'),(144,'Lukovit'),(145,'Lyaskovets'),(146,'Lyubimets'),(147,'Madan'),(148,'Madzharovo'),(149,'Maglizh'),(150,'Makresh'),(151,'Malko Tarnovo'),(152,'Medkovets'),(153,'Medovene'),(154,'Mezdra'),(155,'Mineralni Bani'),(156,'Mirkovo'),(157,'Mizia'),(158,'Montana'),(159,'Nedelino'),(160,'Nesebar'),(161,'Nevestino'),(162,'Nikolaevo'),(163,'Nikopol'),(164,'Nova Zagora'),(165,'Novi Pazar'),(166,'Novo Selo'),(167,'Obzor'),(168,'Omurtag'),(169,'Opaka'),(170,'Oryahovo'),(171,'Panagyurishte'),(172,'Parvomay'),(173,'Parvomaytsi'),(174,'Pavel Banya'),(175,'Pavlikeni'),(176,'Pazardzhik'),(177,'Pernik'),(178,'Perushtitsa'),(179,'Peshtera'),(180,'Petrich'),(181,'Pirdop'),(182,'Pleven'),(183,'Pliska'),(184,'Plovdiv'),(185,'Polski Trambesh'),(186,'Pomorie'),(187,'Popovo'),(188,'Pordim'),(189,'Pravets'),(190,'Primorsko'),(191,'Provadia'),(192,'Radnevo'),(193,'Radomir'),(194,'Rakitovo'),(195,'Rakovski'),(196,'Ravda'),(197,'Razgrad'),(198,'Razlog'),(199,'Rila'),(200,'Roman'),(201,'Rudozem'),(202,'Ruen'),(203,'Ruse'),(204,'Ruzhintsi'),(205,'Sadovo'),(206,'Saedinenie'),(207,'Samokov'),(208,'Samuil'),(209,'Sandanski'),(210,'Sapareva Banya'),(211,'Sarafovo'),(212,'Sarnitsa'),(213,'Satovcha'),(214,'Senovo'),(215,'Septemvri'),(216,'Sevlievo'),(217,'Shabla'),(218,'Shipka'),(219,'Shumen'),(220,'Silistra'),(221,'Simeonovgrad'),(222,'Simitli'),(223,'Sitovo'),(224,'Slavyanovo'),(225,'Sliven'),(226,'Slivnitsa'),(227,'Slivo Pole'),(228,'Smolyan'),(229,'Smyadovo'),(230,'Sofia'),(231,'Sozopol'),(232,'Spasovo'),(233,'Sredets'),(234,'Stamboliyski'),(235,'Stara Kresna'),(236,'Stara Zagora'),(237,'Straldzha'),(238,'Strazhitsa'),(239,'Strelcha'),(240,'Strumyani'),(241,'Suhindol'),(242,'Sungurlare'),(243,'Suvorovo'),(244,'Sveti Vlas'),(245,'Svilengrad'),(246,'Svishtov'),(247,'Svoge'),(248,'Targovishte'),(249,'Tervel'),(250,'Teteven'),(251,'Tochilari'),(252,'Topolovgrad'),(253,'Topolovo'),(254,'Tran'),(255,'Troyan'),(256,'Tryavna'),(257,'Tsar Kaloyan'),(258,'Tsarevo'),(259,'Tsenovo'),(260,'Tutrakan'),(261,'Tvarditsa'),(262,'Ugarchin'),(263,'Valchedram'),(264,'Valchidol'),(265,'Varbitsa'),(266,'Varna'),(267,'Varshets'),(268,'Veliki Preslav'),(269,'Veliko Tŭrnovo'),(270,'Velingrad'),(271,'Venets'),(272,'Vetovo'),(273,'Vetrino'),(274,'Vidin'),(275,'Vratsa'),(276,'Yablanitsa'),(277,'Yakimovo'),(278,'Yakoruda'),(279,'Yambol'),(280,'Zavet'),(281,'Zelenikovo'),(282,'Zemen'),(283,'Zlataritsa'),(284,'Zlatitsa'),(285,'Zlatni Pyasatsi'),(286,'Zlatograd');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(1024) NOT NULL,
  `name` varchar(45) NOT NULL,
  `info` varchar(1024) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_companies_cities1_idx` (`city_id`),
  CONSTRAINT `fk_companies_cities1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'microsoft','9fbf261b62c1d7c00db73afb81dd97fdf20b3442e36e338cb9359b856a03bdc8','Microsoft','Some info about Microsoft',113),(2,'google','bbdefa2950f49882f295b1285d4fa9dec45fc4144bfb07ee6acc68762d12c2e3','Google','Some info about Google',141),(3,'apple','3a7bd3e2360a3d29eea436fcfb7e44c735d117c42d1c1835420b6b9942dd4f1b','Apple','Some info about Apple',55);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_contacts`
--

DROP TABLE IF EXISTS `company_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `website` varchar(45) DEFAULT NULL,
  `linkedin` varchar(45) DEFAULT NULL,
  `facebook` varchar(45) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `phone_number_UNIQUE` (`phone_number`),
  UNIQUE KEY `website_UNIQUE` (`website`),
  UNIQUE KEY `linkedin_UNIQUE` (`linkedin`),
  UNIQUE KEY `facebook_UNIQUE` (`facebook`),
  UNIQUE KEY `twitter_UNIQUE` (`twitter`),
  KEY `fk_contacts_companies1_idx` (`company_id`),
  CONSTRAINT `fk_contacts_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_contacts`
--

LOCK TABLES `company_contacts` WRITE;
/*!40000 ALTER TABLE `company_contacts` DISABLE KEYS */;
INSERT INTO `company_contacts` VALUES (1,'+359888 808 809','microsoft@microsoft.com','http://microsoft.com',NULL,NULL,NULL,1),(2,'+359888 808 801','apple@apple.com','http://apple.com',NULL,NULL,NULL,3),(3,'+359888 808 810','google@google.com','https://google.com/',NULL,NULL,NULL,2);
/*!40000 ALTER TABLE `company_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_ads`
--

DROP TABLE IF EXISTS `job_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_salary` int(11) DEFAULT NULL,
  `max_salary` int(11) DEFAULT NULL,
  `description` varchar(512) NOT NULL,
  `remote` tinyint(4) NOT NULL DEFAULT 1,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `company_id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_job_ads_companies_idx` (`company_id`),
  KEY `fk_job_ads_cities1_idx` (`city_id`),
  CONSTRAINT `fk_job_ads_cities1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ads_companies` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_ads`
--

LOCK TABLES `job_ads` WRITE;
/*!40000 ALTER TABLE `job_ads` DISABLE KEYS */;
INSERT INTO `job_ads` VALUES (1,1500,2500,'Description for Microsoft Job Ad 1',1,1,1,113),(2,1500,2500,'Description for Google Job Ad 1',1,1,2,141),(3,1500,2500,'Description for Apple Job Ad 1',0,1,3,55);
/*!40000 ALTER TABLE `job_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_ads_match_requests`
--

DROP TABLE IF EXISTS `job_ads_match_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_ads_match_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_ad_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `professional_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_job_ads_match_requests_job_ads1_idx` (`job_ad_id`),
  KEY `fk_job_ads_match_requests_companies1_idx` (`company_id`),
  KEY `fk_job_ads_match_requests_professionals1_idx` (`professional_id`),
  CONSTRAINT `fk_job_ads_match_requests_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ads_match_requests_job_ads1` FOREIGN KEY (`job_ad_id`) REFERENCES `job_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ads_match_requests_professionals1` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_ads_match_requests`
--

LOCK TABLES `job_ads_match_requests` WRITE;
/*!40000 ALTER TABLE `job_ads_match_requests` DISABLE KEYS */;
INSERT INTO `job_ads_match_requests` VALUES (1,1,1,2),(2,2,2,1);
/*!40000 ALTER TABLE `job_ads_match_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_ads_skillsets`
--

DROP TABLE IF EXISTS `job_ads_skillsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_ads_skillsets` (
  `job_ad_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `skill_level_id` int(11) NOT NULL,
  PRIMARY KEY (`job_ad_id`,`skill_id`,`skill_level_id`),
  KEY `fk_job_ads_skillsets_skill_levels_skills_skill_levels1_idx` (`skill_id`,`skill_level_id`),
  KEY `fk_job_ads_skillsets_skill_levels_job_ads1_idx` (`job_ad_id`),
  CONSTRAINT `fk_job_ads_skillsets_skill_levels_job_ads1` FOREIGN KEY (`job_ad_id`) REFERENCES `job_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ads_skillsets_skill_levels_skills_skill_levels1` FOREIGN KEY (`skill_id`, `skill_level_id`) REFERENCES `skills_skill_levels` (`skill_id`, `skill_level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_ads_skillsets`
--

LOCK TABLES `job_ads_skillsets` WRITE;
/*!40000 ALTER TABLE `job_ads_skillsets` DISABLE KEYS */;
INSERT INTO `job_ads_skillsets` VALUES (1,1,8),(1,2,3),(2,1,8),(2,2,3),(3,2,3),(3,3,4);
/*!40000 ALTER TABLE `job_ads_skillsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_ads`
--

DROP TABLE IF EXISTS `professional_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professional_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_salary` int(11) DEFAULT NULL,
  `max_salary` int(11) DEFAULT NULL,
  `description` varchar(512) NOT NULL,
  `remote` tinyint(4) NOT NULL DEFAULT 1,
  `professional_id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `ad_state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_professional_ads_professionals1_idx` (`professional_id`),
  KEY `fk_professional_ads_cities1_idx` (`city_id`),
  KEY `fk_professional_ads_ad_states1_idx` (`ad_state_id`),
  CONSTRAINT `fk_professional_ads_ad_states1` FOREIGN KEY (`ad_state_id`) REFERENCES `ad_states` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professional_ads_cities1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professional_ads_professionals1` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_ads`
--

LOCK TABLES `professional_ads` WRITE;
/*!40000 ALTER TABLE `professional_ads` DISABLE KEYS */;
INSERT INTO `professional_ads` VALUES (1,1500,2500,'Description for Doncho Donchev Prof Ad 1',1,1,113,1),(2,1500,2500,'Description for Ivanka Ivancheva Prof Ad 1',1,2,141,1),(3,1500,2500,'Description for Marko Markov Prof Ad 1',1,3,113,1);
/*!40000 ALTER TABLE `professional_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_ads_match_requests`
--

DROP TABLE IF EXISTS `professional_ads_match_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professional_ads_match_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `professional_ad_id` int(11) NOT NULL,
  `professional_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_professional_ads_match_requests_professional_ads1_idx` (`professional_ad_id`),
  KEY `fk_professional_ads_match_requests_professionals1_idx` (`professional_id`),
  KEY `fk_professional_ads_match_requests_companies1_idx` (`company_id`),
  CONSTRAINT `fk_professional_ads_match_requests_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professional_ads_match_requests_professional_ads1` FOREIGN KEY (`professional_ad_id`) REFERENCES `professional_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professional_ads_match_requests_professionals1` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_ads_match_requests`
--

LOCK TABLES `professional_ads_match_requests` WRITE;
/*!40000 ALTER TABLE `professional_ads_match_requests` DISABLE KEYS */;
INSERT INTO `professional_ads_match_requests` VALUES (1,3,3,3);
/*!40000 ALTER TABLE `professional_ads_match_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_ads_skillsets`
--

DROP TABLE IF EXISTS `professional_ads_skillsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professional_ads_skillsets` (
  `professional_ad_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `skill_level_id` int(11) NOT NULL,
  PRIMARY KEY (`professional_ad_id`,`skill_id`,`skill_level_id`),
  KEY `fk_professional_ads_skillsets_skill_levels_skills_skill_le_idx` (`skill_id`,`skill_level_id`),
  KEY `fk_professional_ads_skillsets_skill_levels_professional_ad_idx` (`professional_ad_id`),
  CONSTRAINT `fk_professional_ads_skillsets_skill_levels_professional_ads1` FOREIGN KEY (`professional_ad_id`) REFERENCES `professional_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professional_ads_skillsets_skill_levels_skills_skill_leve1` FOREIGN KEY (`skill_id`, `skill_level_id`) REFERENCES `skills_skill_levels` (`skill_id`, `skill_level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_ads_skillsets`
--

LOCK TABLES `professional_ads_skillsets` WRITE;
/*!40000 ALTER TABLE `professional_ads_skillsets` DISABLE KEYS */;
INSERT INTO `professional_ads_skillsets` VALUES (1,1,8),(1,2,3),(2,1,8),(2,2,3),(3,2,3),(3,3,4);
/*!40000 ALTER TABLE `professional_ads_skillsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_contacts`
--

DROP TABLE IF EXISTS `professional_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professional_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `website` varchar(45) DEFAULT NULL,
  `linkedin` varchar(45) DEFAULT NULL,
  `facebook` varchar(45) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `professional_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `phone_number_UNIQUE` (`phone_number`),
  UNIQUE KEY `website_UNIQUE` (`website`),
  UNIQUE KEY `linkedin_UNIQUE` (`linkedin`),
  UNIQUE KEY `facebook_UNIQUE` (`facebook`),
  UNIQUE KEY `twitter_UNIQUE` (`twitter`),
  KEY `fk_professional_contacts_professionals1_idx` (`professional_id`),
  CONSTRAINT `fk_professional_contacts_professionals1` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_contacts`
--

LOCK TABLES `professional_contacts` WRITE;
/*!40000 ALTER TABLE `professional_contacts` DISABLE KEYS */;
INSERT INTO `professional_contacts` VALUES (1,'+359888 808 809','donchev@mail.com',NULL,NULL,NULL,NULL,1),(2,'+359888 808 808','ivancheva@mail.com',NULL,NULL,NULL,NULL,2),(3,'+359888 808 819','markov@mail.com',NULL,NULL,NULL,NULL,3);
/*!40000 ALTER TABLE `professional_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professionals`
--

DROP TABLE IF EXISTS `professionals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professionals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(1024) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `info` varchar(512) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `city_id` int(11) NOT NULL,
  `main_ad_id` int(11) DEFAULT NULL,
  `hide_matches` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_professionals_cities1_idx` (`city_id`),
  KEY `fk_professionals_professional_ads1_idx` (`main_ad_id`),
  CONSTRAINT `fk_professionals_cities1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professionals_professional_ads1` FOREIGN KEY (`main_ad_id`) REFERENCES `professional_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professionals`
--

LOCK TABLES `professionals` WRITE;
/*!40000 ALTER TABLE `professionals` DISABLE KEYS */;
INSERT INTO `professionals` VALUES (1,'doncho','5568cca2f537dd7b6be0e728867ad23f67de63f5b22202efba58881b8633234c','Doncho','Donchev','Some info about Doncho Donchev',0,230,NULL,0),(2,'ivanka','1320eae11fd9a982b53c27c9a2af4e95ccb92e6ebaf053717a5629725545cf11','Ivanka','Ivancheva','Some info about Ivanka Ivanka',0,99,NULL,0),(3,'marko','8c5faf36ce0dae48351f5e09c5133fdaddcf52d9baf4369db027766a12c1742f','Marko','Markov','Some info about Marko Markov',0,230,NULL,0);
/*!40000 ALTER TABLE `professionals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_categories`
--

DROP TABLE IF EXISTS `skill_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_categories`
--

LOCK TABLES `skill_categories` WRITE;
/*!40000 ALTER TABLE `skill_categories` DISABLE KEYS */;
INSERT INTO `skill_categories` VALUES (1,'programming'),(2,'language'),(3,'marketing'),(4,'administration');
/*!40000 ALTER TABLE `skill_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_levels`
--

DROP TABLE IF EXISTS `skill_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `level_UNIQUE` (`level_name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_levels`
--

LOCK TABLES `skill_levels` WRITE;
/*!40000 ALTER TABLE `skill_levels` DISABLE KEYS */;
INSERT INTO `skill_levels` VALUES (3,'advanced'),(1,'basic'),(7,'expert'),(2,'intermidiate'),(9,'junior developer'),(12,'leader'),(10,'mid-level developer'),(5,'novice'),(6,'proficient'),(11,'senior developer'),(4,'trainee'),(8,'trainee developer');
/*!40000 ALTER TABLE `skill_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `skill_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_skills_skill_categories1_idx` (`skill_category_id`),
  CONSTRAINT `fk_skills_skill_categories1` FOREIGN KEY (`skill_category_id`) REFERENCES `skill_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (1,'python',1),(2,'english',2),(3,'copywriting',3),(4,'SQL',1),(5,'C#',1);
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills_skill_levels`
--

DROP TABLE IF EXISTS `skills_skill_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skills_skill_levels` (
  `skill_id` int(11) NOT NULL,
  `skill_level_id` int(11) NOT NULL,
  PRIMARY KEY (`skill_id`,`skill_level_id`),
  KEY `fk_skills_skill_levels_skill_levels1_idx` (`skill_level_id`),
  KEY `fk_skills_skill_levels_skills1_idx` (`skill_id`),
  CONSTRAINT `fk_skills_skill_levels_skill_levels1` FOREIGN KEY (`skill_level_id`) REFERENCES `skill_levels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_skills_skill_levels_skills1` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills_skill_levels`
--

LOCK TABLES `skills_skill_levels` WRITE;
/*!40000 ALTER TABLE `skills_skill_levels` DISABLE KEYS */;
INSERT INTO `skills_skill_levels` VALUES (1,8),(1,9),(1,10),(1,11),(2,1),(2,2),(2,3),(3,4),(3,5),(3,6),(3,7),(4,8),(4,9),(4,10),(4,11),(5,8),(5,9),(5,10),(5,11);
/*!40000 ALTER TABLE `skills_skill_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `successfull_matches`
--

DROP TABLE IF EXISTS `successfull_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `successfull_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `professional_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_successfull_matches_professionals1_idx` (`professional_id`),
  KEY `fk_successfull_matches_companies1_idx` (`company_id`),
  CONSTRAINT `fk_successfull_matches_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_successfull_matches_professionals1` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `successfull_matches`
--

LOCK TABLES `successfull_matches` WRITE;
/*!40000 ALTER TABLE `successfull_matches` DISABLE KEYS */;
/*!40000 ALTER TABLE `successfull_matches` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-16 13:30:01
