
from os import path
import re
from city_files_generator.public_api_collection import load_data_from_api, convert_to_list
from city_files_generator.collection_sql import create_insert_sql_file


URL = 'https://parseapi.back4app.com/classes/City?limit=300&order=name&keys=name'
HEADERS = {
    'X-Parse-Application-Id': '4yTwrEA5b7FTfTCibwEkzmwLElLfoIQLsNFjM9YD', # This is the fake app's application id
    'X-Parse-Master-Key': 'mLN4Ea6W0F08f5mMvI2yT2lnj1rXB1IJtWKRvRhm' # This is the fake app's readonly master key
}

DIR_PATH = path.dirname(__file__)
SQL_PATH = path.join(DIR_PATH, "city-names.sql")

def create_file(URL, HEADERS, SQL_PATH):
    data = load_data_from_api(URL, HEADERS)
    x = convert_to_list(data)
    create_insert_sql_file(sorted(x), len(x), SQL_PATH)

create_file(URL, HEADERS, SQL_PATH)