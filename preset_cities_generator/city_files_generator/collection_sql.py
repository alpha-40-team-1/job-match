
def create_insert_sql_file(collection, collection_length, path):
    with open(path, "w", encoding='utf-8') as outfile:
        outfile.write('use job_match_api;\nINSERT INTO cities(name) VALUES\n')
        for index, item in enumerate(collection):
            line = f"('{item}')"
            if index < collection_length - 1:
                line = f'{line},\n'
            else:
                line = f'{line};'
            outfile.write(line)