import re
import requests


def load_data_from_api(url, headers):
    data = requests.get(url, headers=headers).content.decode('utf-8')
    return data

def convert_to_list(data):
    regex = r"(?<=name\":\")(.*?)(?=\",\"createdAt)"
    x = set(re.findall(regex, data))
    x = list(x)
    return x
